# epusher-spring-boot-starter


#### 安装教程
[![](https://www.jitpack.io/v/com.gitee.lp-yly/epusher-spring-boot-starter.svg)](https://www.jitpack.io/#com.gitee.lp-yly/epusher-spring-boot-starter)

 **- maven** 
```xml
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://www.jitpack.io</url>
    </repository>
</repositories>

<dependency>
    <groupId>com.gitee.lp-yly</groupId>
    <artifactId>epusher-spring-boot-starter</artifactId>
    <version>LATEST</version>
</dependency>
```
 **- gradle** 
```gradle
allprojects {
    repositories {
        ...
        maven { url 'https://www.jitpack.io' }
    }
}

dependencies {
    implementation 'com.gitee.lp-yly:epusher-spring-boot-starter:LATEST'
}
```
#### 使用说明

1.  WECHAT

```
epusher.wechat.api=http://127.0.0.1:8180/workwechat/#只有代开发应用才需要配置
epusher.wechat.corp-id=
epusher.wechat.agent-id=
epusher.wechat.secret=
```
```
@Resource
private WechatService wechatService;
```
2. DINGDING

```
epusher.dingding.agent-id=
epusher.dingding.app-key=
epusher.dingding.app-secret=
```
```
@Resource
private DingDingService dingDingtService;
```




