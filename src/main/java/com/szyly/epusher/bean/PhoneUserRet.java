package com.szyly.epusher.bean;

public class PhoneUserRet {
    private long errcode;
    private String errmsg;
    private String userid;

    public PhoneUserRet(long errcode, String errmsg) {
        this.errcode = errcode;
        this.errmsg = errmsg;
    }

    public PhoneUserRet() {
    }


    public long getErrcode() {
        return errcode;
    }

    public String getUserid() {
        return userid;
    }

    public PhoneUserRet setUserid(String userid) {
        this.userid = userid;
        return this;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public PhoneUserRet setErrmsg(String errmsg) {
        this.errmsg = errmsg;
        return this;
    }

    public PhoneUserRet setErrcode(long errcode) {
        this.errcode = errcode;
        return this;
    }
}
