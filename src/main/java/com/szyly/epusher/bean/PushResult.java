package com.szyly.epusher.bean;


/**
 * @author PC01
 */
public class PushResult {

    private long errCode;
    private String errMsg;

    @Override
    public String toString() {
        return errCode + " " + errMsg;
    }

    public PushResult() {
    }

    public PushResult(long errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    public long getErrCode() {
        return errCode;
    }

    public PushResult setErrCode(long errCode) {
        this.errCode = errCode;
        return this;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public PushResult setErrMsg(String errMsg) {
        this.errMsg = errMsg;
        return this;
    }
}
