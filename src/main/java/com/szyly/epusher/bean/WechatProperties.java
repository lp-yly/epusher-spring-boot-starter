package com.szyly.epusher.bean;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("epusher.wechat")
public class WechatProperties {


    private String api;

    private String corpId;
    private int agentId;
    private String secret;

    public String getApi() {
        return api;
    }

    public WechatProperties setApi(String api) {
        this.api = api;
        return this;
    }

    public String getCorpId() {
        return corpId;
    }

    public WechatProperties setCorpId(String corpId) {
        this.corpId = corpId;
        return this;
    }

    public int getAgentId() {
        return agentId;
    }

    public WechatProperties setAgentId(int agentId) {
        this.agentId = agentId;
        return this;
    }

    public String getSecret() {
        return secret;
    }

    public WechatProperties setSecret(String secret) {
        this.secret = secret;
        return this;
    }
}
