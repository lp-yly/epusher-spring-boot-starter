package com.szyly.epusher.bean;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("epusher.dingding")
public class DingDingProperties {



    private String appKey;
    private long agentId;
    public String appSecret;

    public String getAppKey() {
        return appKey;
    }

    public DingDingProperties setAppKey(String appKey) {
        this.appKey = appKey;
        return this;
    }

    public long getAgentId() {
        return agentId;
    }

    public DingDingProperties setAgentId(long agentId) {
        this.agentId = agentId;
        return this;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public DingDingProperties setAppSecret(String appSecret) {
        this.appSecret = appSecret;
        return this;
    }
}
