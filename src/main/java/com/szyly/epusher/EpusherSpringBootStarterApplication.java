package com.szyly.epusher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EpusherSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(EpusherSpringBootStarterApplication.class, args);
    }

}
