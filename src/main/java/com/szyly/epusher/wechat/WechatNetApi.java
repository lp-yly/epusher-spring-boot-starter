package com.szyly.epusher.wechat;

import com.szyly.epusher.bean.PhoneUserRet;
import com.szyly.epusher.wechat.bean.*;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

import java.util.concurrent.TimeUnit;

public interface WechatNetApi {
  @GET("gettoken")
  Call<TokenResult> getToken(@Query("corpid") String corpId, @Query("corpsecret") String corpSecret);
  @POST("user/getuserid")
  Call<PhoneUserRet> getUserId(@Query("access_token") String accessToken, @Body PhoneUserReq phoneUserReq);

  @GET("user/get")
  Call<UserInfoRet> getUserInfoByUserId(@Query("access_token") String accessToken, @Query("userid") String userid);
  @POST("message/send")
  Call<MsgResult> sendMessage(@Query("access_token") String accessToken, @Body WechatMsg wechatMsg);


  class Factory{
    public static WechatNetApi create(){
      OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
              .connectTimeout(10, TimeUnit.SECONDS)
              .readTimeout(10, TimeUnit.SECONDS);
      return new Retrofit.Builder()
              .baseUrl("https://qyapi.weixin.qq.com/cgi-bin/")
              .client(clientBuilder.build())
              .addConverterFactory(GsonConverterFactory.create())
              .build().create(WechatNetApi.class);
    }
  }
}