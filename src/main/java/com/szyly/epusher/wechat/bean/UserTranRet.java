package com.szyly.epusher.wechat.bean;

import java.util.List;

public class UserTranRet {

    private int errcode;
    private String errmsg;
    private List<OpenUseridListDTO> open_userid_list;
    private List<String> invalid_userid_list;

    @Override
    public String toString() {
        return "UserTranRet{" +
                "errcode=" + errcode +
                ", errmsg='" + errmsg + '\'' +
                ", open_userid_list=" + open_userid_list +
                ", invalid_userid_list=" + invalid_userid_list +
                '}';
    }

    public int getErrcode() {
        return errcode;
    }

    public UserTranRet setErrcode(int errcode) {
        this.errcode = errcode;
        return this;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public UserTranRet setErrmsg(String errmsg) {
        this.errmsg = errmsg;
        return this;
    }

    public List<OpenUseridListDTO> getOpen_userid_list() {
        return open_userid_list;
    }

    public UserTranRet setOpen_userid_list(List<OpenUseridListDTO> open_userid_list) {
        this.open_userid_list = open_userid_list;
        return this;
    }

    public List<String> getInvalid_userid_list() {
        return invalid_userid_list;
    }

    public UserTranRet setInvalid_userid_list(List<String> invalid_userid_list) {
        this.invalid_userid_list = invalid_userid_list;
        return this;
    }

    public static class OpenUseridListDTO {
        private String userid;
        private String open_userid;

        @Override
        public String toString() {
            return "OpenUseridListDTO{" +
                    "userid='" + userid + '\'' +
                    ", open_userid='" + open_userid + '\'' +
                    '}';
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getOpen_userid() {
            return open_userid;
        }

        public void setOpen_userid(String open_userid) {
            this.open_userid = open_userid;
        }
    }
}
