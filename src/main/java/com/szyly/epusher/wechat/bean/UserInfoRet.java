package com.szyly.epusher.wechat.bean;

import java.util.List;

public class UserInfoRet {
    private int errcode;
    private String errmsg;
    private String userid;
    private String name;
    private List<Integer> department;
    private Integer status;
    private Integer isleader;
    private String english_name;
    private Integer enable;
    private Integer hide_mobile;
    private List<Integer> order;
    private Integer main_department;
    private String alias;
    private List<Integer> is_leader_in_dept;
    private List<?> direct_leader;

    public int getErrcode() {
        return errcode;
    }

    public UserInfoRet() {
    }

    public UserInfoRet(int errcode, String errmsg) {
        this.errcode = errcode;
        this.errmsg = errmsg;
    }



    public String getErrmsg() {
        return errmsg;
    }

    public UserInfoRet setErrcode(int errcode) {
        this.errcode = errcode;
        return this;
    }

    public UserInfoRet setErrmsg(String errmsg) {
        this.errmsg = errmsg;
        return this;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getDepartment() {
        return department;
    }

    public void setDepartment(List<Integer> department) {
        this.department = department;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsleader() {
        return isleader;
    }

    public void setIsleader(Integer isleader) {
        this.isleader = isleader;
    }

    public String getEnglish_name() {
        return english_name;
    }

    public void setEnglish_name(String english_name) {
        this.english_name = english_name;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Integer getHide_mobile() {
        return hide_mobile;
    }

    public void setHide_mobile(Integer hide_mobile) {
        this.hide_mobile = hide_mobile;
    }

    public List<Integer> getOrder() {
        return order;
    }

    public void setOrder(List<Integer> order) {
        this.order = order;
    }

    public Integer getMain_department() {
        return main_department;
    }

    public void setMain_department(Integer main_department) {
        this.main_department = main_department;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public List<Integer> getIs_leader_in_dept() {
        return is_leader_in_dept;
    }

    public void setIs_leader_in_dept(List<Integer> is_leader_in_dept) {
        this.is_leader_in_dept = is_leader_in_dept;
    }

    public List<?> getDirect_leader() {
        return direct_leader;
    }

    public void setDirect_leader(List<?> direct_leader) {
        this.direct_leader = direct_leader;
    }
}
