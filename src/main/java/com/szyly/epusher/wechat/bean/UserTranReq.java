package com.szyly.epusher.wechat.bean;

import java.util.List;

public class UserTranReq {
    private List<String> userid_list;

    public UserTranReq(List<String> userid_list) {
        this.userid_list = userid_list;
    }

    public List<String> getUserid_list() {
        return userid_list;
    }

    public void setUserid_list(List<String> userid_list) {
        this.userid_list = userid_list;
    }
}
