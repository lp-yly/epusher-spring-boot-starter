package com.szyly.epusher.wechat.bean;

import java.util.List;

public class BatchActiveAccountReq {


    private String corpid;
    private List<ActiveListDTO> active_list;

    public BatchActiveAccountReq() {
    }

    public BatchActiveAccountReq(String corpid, List<ActiveListDTO> active_list) {
        this.corpid = corpid;
        this.active_list = active_list;
    }

    public String getCorpid() {
        return corpid;
    }

    public BatchActiveAccountReq setCorpid(String corpid) {
        this.corpid = corpid;
        return this;
    }

    public List<ActiveListDTO> getActive_list() {
        return active_list;
    }

    public BatchActiveAccountReq setActive_list(List<ActiveListDTO> active_list) {
        this.active_list = active_list;
        return this;
    }

    public static class ActiveListDTO {

        private String active_code;
        private String userid;

        public ActiveListDTO() {
        }

        public ActiveListDTO(String active_code, String userid) {
            this.active_code = active_code;
            this.userid = userid;
        }

        public String getActive_code() {
            return active_code;
        }

        public void setActive_code(String active_code) {
            this.active_code = active_code;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }
    }
}
