package com.szyly.epusher.wechat.bean;

import java.util.List;

public class BatchActiveAccountRet {


    private int errcode;
    private String errmsg;
    private List<ActiveResultDTO> active_result;

    public BatchActiveAccountRet() {
    }

    public BatchActiveAccountRet(int errcode, String errmsg) {
        this.errcode = errcode;
        this.errmsg = errmsg;
    }

    public int getErrcode() {
        return errcode;
    }

    public BatchActiveAccountRet setErrcode(int errcode) {
        this.errcode = errcode;
        return this;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public BatchActiveAccountRet setErrmsg(String errmsg) {
        this.errmsg = errmsg;
        return this;
    }

    public List<ActiveResultDTO> getActive_result() {
        return active_result;
    }

    public void setActive_result(List<ActiveResultDTO> active_result) {
        this.active_result = active_result;
    }

    public static class ActiveResultDTO {
        private String userid;
        private String active_code;
        private Integer errcode;

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getActive_code() {
            return active_code;
        }

        public void setActive_code(String active_code) {
            this.active_code = active_code;
        }

        public Integer getErrcode() {
            return errcode;
        }

        public void setErrcode(Integer errcode) {
            this.errcode = errcode;
        }
    }
}
