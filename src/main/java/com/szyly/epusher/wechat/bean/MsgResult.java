package com.szyly.epusher.wechat.bean;

public class MsgResult {

        /**
         * errcode : 0
         * errmsg : ok
         */

        private int errcode;
        private String errmsg;

        @Override
        public String toString() {
            return "MsgResult{" +
                    "errcode=" + errcode +
                    ", errmsg='" + errmsg + '\'' +
                    '}';
        }

        public int getErrcode() {
            return errcode;
        }

        public void setErrcode(int errcode) {
            this.errcode = errcode;
        }

        public String getErrmsg() {
            return errmsg;
        }

        public void setErrmsg(String errmsg) {
            this.errmsg = errmsg;
        }
    }
