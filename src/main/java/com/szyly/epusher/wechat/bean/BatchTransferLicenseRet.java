package com.szyly.epusher.wechat.bean;

import java.util.List;

public class BatchTransferLicenseRet {
    private int errcode;
    private String errmsg;
    private List<TransferResultDTO> transfer_result;

    public BatchTransferLicenseRet() {
    }

    public BatchTransferLicenseRet(int errcode, String errmsg) {
        this.errcode = errcode;
        this.errmsg = errmsg;
    }

    public int getErrcode() {
        return errcode;
    }

    public BatchTransferLicenseRet setErrcode(int errcode) {
        this.errcode = errcode;
        return this;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public BatchTransferLicenseRet setErrmsg(String errmsg) {
        this.errmsg = errmsg;
        return this;
    }

    public List<TransferResultDTO> getTransfer_result() {
        return transfer_result;
    }

    public void setTransfer_result(List<TransferResultDTO> transfer_result) {
        this.transfer_result = transfer_result;
    }

    public static class TransferResultDTO {
        private String handover_userid;
        private String takeover_userid;
        private Integer errcode;

        public String getHandover_userid() {
            return handover_userid;
        }

        public void setHandover_userid(String handover_userid) {
            this.handover_userid = handover_userid;
        }

        public String getTakeover_userid() {
            return takeover_userid;
        }

        public void setTakeover_userid(String takeover_userid) {
            this.takeover_userid = takeover_userid;
        }

        public Integer getErrcode() {
            return errcode;
        }

        public void setErrcode(Integer errcode) {
            this.errcode = errcode;
        }
    }
}
