package com.szyly.epusher.wechat.bean;

import java.util.List;

public class WechatMsg {



    /**
     * touser : ["userid1","userid2","CorpId1/userid1","CorpId2/userid2"]
     * toparty : ["partyid1","partyid2","LinkedId1/partyid1","LinkedId2/partyid2"]
     * totag : ["tagid1","tagid2"]
     * toall : 0
     * msgtype : text
     * agentid : 1
     * text : {"content":"你的快递已到，请携带工卡前往邮件中心领取。\n出发前可查看<a href=\"http://work.weixin.qq.com\">邮件中心视频实况<\/a>，聪明避开排队。"}
     * safe : 0
     */

    private int toall;
    private String msgtype = "markdown";
    private int agentid;
    private TextBean markdown;
    private int safe;
    private String touser;
    private String toparty;
    private List<String> totag;

    private TextBean text;
    private Integer enable_id_trans;
    private Integer enable_duplicate_check;
    private Integer duplicate_check_interval;
    private ImageDTO image;
    private ImageDTO voice;
    private VideoDTO video;
    private VideoDTO file;
    private TextcardDTO textcard;
    private NewsDTO news;
    private MpnewsDTO mpnews;
    private MiniprogramNoticeDTO miniprogram_notice;


    public int getToall() {
        return toall;
    }

    public WechatMsg setToall(int toall) {
        this.toall = toall;
        return this;
    }

    public String getMsgtype() {
        return msgtype;
    }

    public WechatMsg setMsgtype(String msgtype) {
        this.msgtype = msgtype;
        return this;
    }

    public int getAgentid() {
        return agentid;
    }

    public WechatMsg setAgentid(int agentid) {
        this.agentid = agentid;
        return this;
    }


    public int getSafe() {
        return safe;
    }

    public WechatMsg setSafe(int safe) {
        this.safe = safe;
        return this;
    }

    public String getTouser() {
        return touser;
    }

    public WechatMsg setTouser(String touser) {
        this.touser = touser;
        return this;
    }

    public String getToparty() {
        return toparty;
    }

    public WechatMsg setToparty(String toparty) {
        this.toparty = toparty;
        return this;
    }

    public List<String> getTotag() {
        return totag;
    }

    public WechatMsg setTotag(List<String> totag) {
        this.totag = totag;
        return this;
    }

    public TextBean getMarkdown() {
        return markdown;
    }

    public WechatMsg setMarkdown(TextBean markdown) {
        this.markdown = markdown;
        return this;
    }


    public TextBean getText() {
        return text;
    }

    public WechatMsg setText(TextBean text) {
        this.text = text;
        return this;
    }

    public Integer getEnable_id_trans() {
        return enable_id_trans;
    }

    public void setEnable_id_trans(Integer enable_id_trans) {
        this.enable_id_trans = enable_id_trans;
    }

    public Integer getEnable_duplicate_check() {
        return enable_duplicate_check;
    }

    public void setEnable_duplicate_check(Integer enable_duplicate_check) {
        this.enable_duplicate_check = enable_duplicate_check;
    }

    public Integer getDuplicate_check_interval() {
        return duplicate_check_interval;
    }

    public void setDuplicate_check_interval(Integer duplicate_check_interval) {
        this.duplicate_check_interval = duplicate_check_interval;
    }

    public ImageDTO getImage() {
        return image;
    }

    public void setImage(ImageDTO image) {
        this.image = image;
    }

    public ImageDTO getVoice() {
        return voice;
    }

    public void setVoice(ImageDTO voice) {
        this.voice = voice;
    }

    public VideoDTO getVideo() {
        return video;
    }

    public void setVideo(VideoDTO video) {
        this.video = video;
    }

    public VideoDTO getFile() {
        return file;
    }

    public void setFile(VideoDTO file) {
        this.file = file;
    }

    public TextcardDTO getTextcard() {
        return textcard;
    }

    public void setTextcard(TextcardDTO textcard) {
        this.textcard = textcard;
    }

    public NewsDTO getNews() {
        return news;
    }

    public void setNews(NewsDTO news) {
        this.news = news;
    }

    public MpnewsDTO getMpnews() {
        return mpnews;
    }

    public void setMpnews(MpnewsDTO mpnews) {
        this.mpnews = mpnews;
    }

    public MiniprogramNoticeDTO getMiniprogram_notice() {
        return miniprogram_notice;
    }

    public void setMiniprogram_notice(MiniprogramNoticeDTO miniprogram_notice) {
        this.miniprogram_notice = miniprogram_notice;
    }

    public static class TextBean {
        /**
         * content : 你的快递已到，请携带工卡前往邮件中心领取。
         * 出发前可查看<a href="http://work.weixin.qq.com">邮件中心视频实况</a>，聪明避开排队。
         */

        private String content;

        public String getContent() {
            return content;
        }

        public TextBean setContent(String content) {
            this.content = content;
            return this;
        }
    }

    public static class ImageDTO {
        private String media_id;

        public String getMedia_id() {
            return media_id;
        }

        public void setMedia_id(String media_id) {
            this.media_id = media_id;
        }
    }

    public static class VideoDTO {
        private String media_id;
        private String title;
        private String description;

        public String getMedia_id() {
            return media_id;
        }

        public void setMedia_id(String media_id) {
            this.media_id = media_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    public static class TextcardDTO {
        private String title;
        private String description;
        private String url;
        private String btntxt;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getBtntxt() {
            return btntxt;
        }

        public void setBtntxt(String btntxt) {
            this.btntxt = btntxt;
        }
    }

    public static class NewsDTO {
        private List<ArticlesDTO> articles;

        public List<ArticlesDTO> getArticles() {
            return articles;
        }

        public void setArticles(List<ArticlesDTO> articles) {
            this.articles = articles;
        }

        public static class ArticlesDTO {
            private String title;
            private String description;
            private String url;
            private String picurl;
            private String appid;
            private String pagepath;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getPicurl() {
                return picurl;
            }

            public void setPicurl(String picurl) {
                this.picurl = picurl;
            }

            public String getAppid() {
                return appid;
            }

            public void setAppid(String appid) {
                this.appid = appid;
            }

            public String getPagepath() {
                return pagepath;
            }

            public void setPagepath(String pagepath) {
                this.pagepath = pagepath;
            }
        }
    }

    public static class MpnewsDTO {
        private List<ArticlesDTO> articles;

        public List<ArticlesDTO> getArticles() {
            return articles;
        }

        public void setArticles(List<ArticlesDTO> articles) {
            this.articles = articles;
        }

        public static class ArticlesDTO {
            private String title;
            private String thumb_media_id;
            private String author;
            private String content_source_url;
            private String content;
            private String digest;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getThumb_media_id() {
                return thumb_media_id;
            }

            public void setThumb_media_id(String thumb_media_id) {
                this.thumb_media_id = thumb_media_id;
            }

            public String getAuthor() {
                return author;
            }

            public void setAuthor(String author) {
                this.author = author;
            }

            public String getContent_source_url() {
                return content_source_url;
            }

            public void setContent_source_url(String content_source_url) {
                this.content_source_url = content_source_url;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getDigest() {
                return digest;
            }

            public void setDigest(String digest) {
                this.digest = digest;
            }
        }
    }

    public static class MiniprogramNoticeDTO {
        private String appid;
        private String page;
        private String title;
        private String description;
        private Boolean emphasis_first_item;
        private List<ContentItemDTO> content_item;

        public String getAppid() {
            return appid;
        }

        public void setAppid(String appid) {
            this.appid = appid;
        }

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Boolean getEmphasis_first_item() {
            return emphasis_first_item;
        }

        public void setEmphasis_first_item(Boolean emphasis_first_item) {
            this.emphasis_first_item = emphasis_first_item;
        }

        public List<ContentItemDTO> getContent_item() {
            return content_item;
        }

        public void setContent_item(List<ContentItemDTO> content_item) {
            this.content_item = content_item;
        }

        public static class ContentItemDTO {
            private String key;
            private String value;

            public String getKey() {
                return key;
            }

            public void setKey(String key) {
                this.key = key;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }
    }
}
