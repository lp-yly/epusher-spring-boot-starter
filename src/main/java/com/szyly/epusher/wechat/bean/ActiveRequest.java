package com.szyly.epusher.wechat.bean;

public class ActiveRequest {
    private String activeCode;
    private String oldUserId;
    private String activeUserId;


    public ActiveRequest() {
    }

    public ActiveRequest(String activeCode, String activeUserId) {
        this.activeCode = activeCode;
        this.activeUserId = activeUserId;
    }

    public ActiveRequest(String activeCode, String oldUserId, String activeUserId) {
        this.activeCode = activeCode;
        this.oldUserId = oldUserId;
        this.activeUserId = activeUserId;
    }

    public String getActiveCode() {
        return activeCode;
    }

    public ActiveRequest setActiveCode(String activeCode) {
        this.activeCode = activeCode;
        return this;
    }

    public String getOldUserId() {
        return oldUserId;
    }

    public ActiveRequest setOldUserId(String oldUserId) {
        this.oldUserId = oldUserId;
        return this;
    }

    public String getActiveUserId() {
        return activeUserId;
    }

    public ActiveRequest setActiveUserId(String activeUserId) {
        this.activeUserId = activeUserId;
        return this;
    }
}
