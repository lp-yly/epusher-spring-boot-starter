package com.szyly.epusher.wechat.bean;

public class PhoneUserReq {

    private String mobile;

    public PhoneUserReq(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

}
