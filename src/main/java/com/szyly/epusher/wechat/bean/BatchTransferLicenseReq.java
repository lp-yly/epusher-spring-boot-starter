package com.szyly.epusher.wechat.bean;

import java.util.List;

public class BatchTransferLicenseReq {
    private String corpid;
    private List<TransferListDTO> transfer_list;

    public BatchTransferLicenseReq() {
    }

    public BatchTransferLicenseReq(String corpid, List<TransferListDTO> transfer_list) {
        this.corpid = corpid;
        this.transfer_list = transfer_list;
    }

    public String getCorpid() {
        return corpid;
    }

    public void setCorpid(String corpid) {
        this.corpid = corpid;
    }

    public List<TransferListDTO> getTransfer_list() {
        return transfer_list;
    }

    public void setTransfer_list(List<TransferListDTO> transfer_list) {
        this.transfer_list = transfer_list;
    }

    public static class TransferListDTO {


        private String handover_userid;
        private String takeover_userid;


        public TransferListDTO() {
        }

        public TransferListDTO(String handover_userid, String takeover_userid) {
            this.handover_userid = handover_userid;
            this.takeover_userid = takeover_userid;
        }

        public String getHandover_userid() {
            return handover_userid;
        }

        public void setHandover_userid(String handover_userid) {
            this.handover_userid = handover_userid;
        }

        public String getTakeover_userid() {
            return takeover_userid;
        }

        public void setTakeover_userid(String takeover_userid) {
            this.takeover_userid = takeover_userid;
        }
    }
}
