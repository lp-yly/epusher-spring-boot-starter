package com.szyly.epusher.wechat.bean;

public interface MessageType {
    String TEXT = "text";
    String MARKDOWN = "markdown";
}
