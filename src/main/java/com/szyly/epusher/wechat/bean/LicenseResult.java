package com.szyly.epusher.wechat.bean;

import java.util.List;

public class LicenseResult {
    private int errCode;
    private String errMsg;
    private List<License> licenses;

    @Override
    public String toString() {
        return "LicenseResult{" +
                "errCode=" + errCode +
                ", errMsg='" + errMsg + '\'' +
                ", licenses=" + licenses +
                '}';
    }

    public LicenseResult() {
    }

    public LicenseResult(String errMsg) {
        this.errMsg = errMsg;
    }


    public LicenseResult(int errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    public int getErrCode() {
        return errCode;
    }

    public LicenseResult setErrCode(int errCode) {
        this.errCode = errCode;
        return this;
    }

    public LicenseResult(List<License> licenses) {
        this.licenses = licenses;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public LicenseResult setErrMsg(String errMsg) {
        this.errMsg = errMsg;
        return this;
    }

    public List<License> getLicenses() {
        return licenses;
    }

    public LicenseResult setLicenses(List<License> licenses) {
        this.licenses = licenses;
        return this;
    }
}
