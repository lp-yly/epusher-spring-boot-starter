package com.szyly.epusher.wechat.bean;

/**
 * @author PC01
 */
public class License {



    private String active_code;
    private Integer type;
    private Integer status;
    private String status_name;

    private String userid;


    private String username;

    private Integer active_time;
    private Integer expire_time;
    private Integer create_time;


    @Override
    public String toString() {
        return "License{" +
                "active_code='" + active_code + '\'' +
                ", type=" + type +
                ", status=" + status +
                ", status_name='" + status_name + '\'' +
                ", userid='" + userid + '\'' +
                ", username='" + username + '\'' +
                ", active_time=" + active_time +
                ", expire_time=" + expire_time +
                ", create_time=" + create_time +
                '}';
    }

    public String getActive_code() {
        return active_code;
    }

    public License setActive_code(String active_code) {
        this.active_code = active_code;
        return this;
    }



    public Integer getType() {
        return type;
    }

    public License setType(Integer type) {
        this.type = type;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public License setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public String getStatus_name() {
        return status_name;
    }

    public License setStatus_name(String status_name) {
        this.status_name = status_name;
        return this;
    }

    public String getUserid() {
        return userid;
    }

    public License setUserid(String userid) {
        this.userid = userid;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public License setUsername(String username) {
        this.username = username;
        return this;
    }

    public Integer getActive_time() {
        return active_time;
    }

    public License setActive_time(Integer active_time) {
        this.active_time = active_time;
        return this;
    }

    public Integer getExpire_time() {
        return expire_time;
    }

    public License setExpire_time(Integer expire_time) {
        this.expire_time = expire_time;
        return this;
    }

    public Integer getCreate_time() {
        return create_time;
    }

    public License setCreate_time(Integer create_time) {
        this.create_time = create_time;
        return this;
    }
}
