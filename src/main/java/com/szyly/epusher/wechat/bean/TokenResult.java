package com.szyly.epusher.wechat.bean;

import java.util.Date;

public class TokenResult {
    private Integer errcode;
    private String errmsg;
    private String access_token;
    private Integer expires_in;

    private Date expired_time;

    @Override
    public String toString() {
        return "TokenResult{" +
                "errcode=" + errcode +
                ", errmsg='" + errmsg + '\'' +
                ", access_token='" + access_token + '\'' +
                ", expires_in=" + expires_in +
                ", expired_time=" + expired_time +
                '}';
    }

    public Date getExpired_time() {
        return expired_time;
    }

    public TokenResult setExpired_time(Date expired_time) {
        this.expired_time = expired_time;
        return this;
    }

    public Integer getErrcode() {
        return errcode;
    }

    public TokenResult setErrcode(Integer errcode) {
        this.errcode = errcode;
        return this;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public TokenResult setErrmsg(String errmsg) {
        this.errmsg = errmsg;
        return this;
    }

    public String getAccess_token() {
        return access_token;
    }

    public TokenResult setAccess_token(String access_token) {
        this.access_token = access_token;
        return this;
    }

    public Integer getExpires_in() {
        return expires_in;
    }

    public TokenResult setExpires_in(Integer expires_in) {
        this.expires_in = expires_in;
        return this;
    }
}
