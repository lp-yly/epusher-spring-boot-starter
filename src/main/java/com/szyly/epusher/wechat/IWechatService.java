package com.szyly.epusher.wechat;

import com.szyly.epusher.bean.PushResult;
import com.szyly.epusher.wechat.bean.ActiveRequest;
import com.szyly.epusher.wechat.bean.LicenseResult;
import com.szyly.epusher.bean.PhoneUserRet;
import com.szyly.epusher.wechat.bean.WechatMsg;

import java.util.List;

public interface IWechatService {
    PhoneUserRet getUserIdByPhoneNumber(String phoneNumber);


    PushResult sendMessage(WechatMsg wechatMsg);

    PushResult sendMessages(List<WechatMsg> wechatMsgs);

    PushResult sendTextMessages(List<String> phoneNumbers, String content);

    PushResult sendTextMessage(String phoneNumber, String content);

    PushResult sendMarkdownMessages(List<String> phoneNumbers, String content);

    PushResult sendMarkdownMessage(String phoneNumber, String content);


    LicenseResult getLicense();

    LicenseResult activeAccounts(List<ActiveRequest> activeRequests);
    LicenseResult activeAccount(ActiveRequest activeRequest);


}