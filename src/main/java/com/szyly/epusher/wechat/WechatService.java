package com.szyly.epusher.wechat;

import com.szyly.epusher.bean.PhoneUserRet;
import com.szyly.epusher.bean.PushResult;
import com.szyly.epusher.bean.WechatProperties;
import com.szyly.epusher.wechat.bean.*;

import java.util.Collections;
import java.util.List;

/**
 * @author PC01
 */
public class WechatService implements IWechatService {
    private final WechatProperties wechatProperties;
    private final WechatApi wechatApi;


    public WechatService(WechatProperties wechatProperties) {
        this.wechatProperties = wechatProperties;
        wechatApi = new WechatApi(wechatProperties);

    }

    public WechatProperties getWechatProperties() {
        return wechatProperties;
    }

    public WechatApi getWechatApi() {
        return wechatApi;
    }

    @Override
    public PhoneUserRet getUserIdByPhoneNumber(String phoneNumber) {
        return wechatApi.getUserIdByPhoneNumber(phoneNumber);
    }

    @Override
    public PushResult sendMessage(WechatMsg wechatMsg) {
        return wechatApi.sendMessage(wechatMsg);
    }

    @Override
    public PushResult sendMessages(List<WechatMsg> wechatMsgs) {
        for (WechatMsg wechatMsg : wechatMsgs) {
            wechatApi.sendMessage(wechatMsg);
        }
        return new PushResult(0, "success");
    }

    @Override
    public PushResult sendTextMessages(List<String> phoneNumbers, String content) {
        return wechatApi.sendPhoneMessages(MessageType.TEXT, phoneNumbers, content);
    }

    @Override
    public PushResult sendTextMessage(String phoneNumber, String content) {
        return wechatApi.sendPhoneMessage(MessageType.TEXT, phoneNumber, content);
    }

    @Override
    public PushResult sendMarkdownMessages(List<String> phoneNumbers, String content) {
        return wechatApi.sendPhoneMessages(MessageType.MARKDOWN, phoneNumbers, content);
    }

    @Override
    public PushResult sendMarkdownMessage(String phoneNumber, String content) {
        return wechatApi.sendPhoneMessage(MessageType.MARKDOWN, phoneNumber, content);
    }

    @Override
    public LicenseResult getLicense() {
        return wechatApi.getLicense();
    }

    @Override
    public LicenseResult activeAccounts(List<ActiveRequest> activeRequests) {
        return wechatApi.activeAccounts(activeRequests);
    }

    @Override
    public LicenseResult activeAccount(ActiveRequest activeRequest) {
        return wechatApi.activeAccounts(Collections.singletonList(activeRequest));
    }


}