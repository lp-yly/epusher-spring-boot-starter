package com.szyly.epusher.wechat;


import com.szyly.epusher.wechat.bean.*;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

import java.util.concurrent.TimeUnit;

public interface DepartmentApi {

    @GET("getLicenses")
    Call<LicenseResult> getLicenses(@Query("corpId") String corpId);

    @POST("batchActiveAccount")
    Call<BatchActiveAccountRet> batchActiveAccount(@Body BatchActiveAccountReq activeAccountReq);

    @POST(value = "batchTransferLicense")
    Call<BatchTransferLicenseRet> batchTransferLicense(@Body BatchTransferLicenseReq batchTransferLicenseReq);

    class Factory {
        public static DepartmentApi create(String api) {
            OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(10, TimeUnit.SECONDS);
            return new Retrofit.Builder()
                    .baseUrl(api)
                    .client(clientBuilder.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(DepartmentApi.class);
        }
    }
}
