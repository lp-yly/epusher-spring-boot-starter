package com.szyly.epusher.config;

import com.szyly.epusher.bean.DingDingProperties;
import com.szyly.epusher.bean.WechatProperties;
import com.szyly.epusher.dingding.DingDingService;
import com.szyly.epusher.wechat.WechatService;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(DingDingProperties.class)
public class DingDingAutoConfiguration {

    private final DingDingProperties dingDingProperties;

    public DingDingAutoConfiguration(DingDingProperties dingDingProperties) {
        this.dingDingProperties = dingDingProperties;
    }

    @Bean
    public DingDingService dingDingService() {
        return new DingDingService(dingDingProperties);
    }

}
