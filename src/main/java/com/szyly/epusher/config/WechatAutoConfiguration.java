package com.szyly.epusher.config;

import com.szyly.epusher.bean.WechatProperties;
import com.szyly.epusher.wechat.WechatService;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(WechatProperties.class)
public class WechatAutoConfiguration {

    private final WechatProperties wechatProperties;

    public WechatAutoConfiguration(WechatProperties wechatProperties) {
        this.wechatProperties = wechatProperties;
    }

    @Bean
    public WechatService wechatService() {
        return new WechatService(wechatProperties);
    }

}
