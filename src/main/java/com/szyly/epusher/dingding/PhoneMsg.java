package com.szyly.epusher.dingding;

public class PhoneMsg {
    private String phone;
    private String content;

    public String getPhone() {
        return phone;
    }

    public PhoneMsg setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getContent() {
        return content;
    }

    public PhoneMsg setContent(String content) {
        this.content = content;
        return this;
    }
}
