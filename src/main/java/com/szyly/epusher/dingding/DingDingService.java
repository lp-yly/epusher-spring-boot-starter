package com.szyly.epusher.dingding;

import com.szyly.epusher.bean.DingDingProperties;
import com.szyly.epusher.bean.PushResult;
import com.szyly.epusher.bean.PhoneUserRet;

import java.util.List;

public class DingDingService implements IDingDingService{
    DingDingApi dingDingApi;
    public DingDingService(DingDingProperties properties) {
        dingDingApi = new DingDingApi(properties);

    }

    @Override
    public PhoneUserRet getUserIdByPhoneNumber(String phoneNumber) {
        return dingDingApi.getUserIdByPhoneNumber(phoneNumber);
    }

    @Override
    public PushResult sendTextMessages(List<String> phoneNumbers, String content) {
        return dingDingApi.sendTextMessages(phoneNumbers,content);
    }

    @Override
    public PushResult sendTextMessage(String phoneNumber, String content) {
        return dingDingApi.sendTextMessageByPhone(phoneNumber,content);
    }

    @Override
    public PushResult sendMarkdownMessages(List<String> phoneNumbers, String title,String content) {
        return dingDingApi.sendMarkdownMessages(phoneNumbers,title,content);
    }

    @Override
    public PushResult sendMarkdownMessage(String phoneNumber,String title, String content) {
        return dingDingApi.sendMarkdownMessageByPhone(phoneNumber,title,content);
    }
}
