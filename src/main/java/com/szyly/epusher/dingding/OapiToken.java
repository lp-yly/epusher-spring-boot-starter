package com.szyly.epusher.dingding;

import com.dingtalk.api.response.OapiGettokenResponse;

public class OapiToken extends OapiGettokenResponse {
    private Long expiresTime;

    public OapiToken(long errcode, String errMsg) {
        setErrcode(errcode);
        setErrmsg(errMsg);
    }


    public OapiToken(Long expiresTime) {
        this.expiresTime = expiresTime;
    }

    public Long getExpiresTime() {
        return expiresTime;
    }

    public OapiToken setExpiresTime(Long expiresTime) {
        this.expiresTime = expiresTime;
        return this;
    }
}
