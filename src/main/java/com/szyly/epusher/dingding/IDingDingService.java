package com.szyly.epusher.dingding;

import com.szyly.epusher.bean.PushResult;
import com.szyly.epusher.bean.PhoneUserRet;

import java.util.List;

public interface IDingDingService {
    PhoneUserRet getUserIdByPhoneNumber(String phoneNumber);




    PushResult sendTextMessages(List<String> phoneNumbers, String content);

    PushResult sendTextMessage(String phoneNumber, String content);

    PushResult sendMarkdownMessages(List<String> phoneNumbers,String title, String content);

    PushResult sendMarkdownMessage(String phoneNumber, String title, String content);



}